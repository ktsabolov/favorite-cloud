assert = require 'assert'
fs = require 'fs'
{ join } = require 'path'

describe 'FavoriteCloud API', ->
  FavoriteCloud = require '../'

  api = new FavoriteCloud(
    username: 'gmd'
    apiKey: 'abddb2f143ed44c19ef68c0778d68b32'
  )

  timestamp = new Date().getTime()
  tempFileName = "test-#{timestamp}.txt"
  tempFilePath = join(__dirname, tempFileName)
  tempFileText = timestamp + ' - THIS IS A TEST FILE IN THE CLOUD'


  describe 'getFilesList', ->
    it 'should return a JSON with meta and files list', (done) ->
      api.getFilesList (err, res) ->
        return done(err) if err
        if res and res.objects and res.meta
          done()


  describe 'createFile', ->
    before (done) ->
      fs.writeFile(tempFilePath, tempFileText, done)

    after (done) ->
      fs.unlink(tempFilePath, done)

    it 'should create file in the cloud', (done) ->
      fileStream = fs.createReadStream(tempFilePath)
      api.createFile(fileStream, (err, res) ->
        return done(err) if err
        if res?.filename == tempFileName
          done()
        else
          done('Uploaded file name doesn\'t match')
      )


  describe 'getFileInfo', ->
    it 'should get existing file info from the cloud', (done) ->
      api.getFileInfo(tempFileName, (err, res) ->
        return done(err) if err

        if res?.filename == tempFileName
          done()
        else
          done('Unexpected result')
      )


  describe 'getFile', ->
    it 'should get existing file from the cloud', (done) ->
      api.getFile(tempFileName, (err, fileStream) ->
        return done(err) if err

        if fileStream
          contents = ''

          fileStream.on 'data', (chunk) ->
            contents += chunk

          fileStream.on 'end', ->
            if contents == tempFileText
              done()
            else
              done("File contents doesn't match")

          fileStream.resume()
      )


  describe 'replaceFile', ->
    replaceFileName = "replace-#{timestamp}.txt"
    replaceFilePath = join(__dirname, replaceFileName)
    replaceFileText = timestamp + ' - THIS IS A REPLACE FILE IN THE CLOUD'

    before (done) ->
      fs.writeFile(replaceFilePath, replaceFileText, done)

    after (done) ->
      fs.unlink(replaceFilePath, done)

    it 'should replace existing file in the cloud', (done) ->
      fileStream = fs.createReadStream(replaceFilePath)
      api.replaceFile(tempFileName, fileStream, (err, res) ->
        return done(err) if err

        # TODO: Check that contents of the file changed
        if res?.filename == tempFileName
          done()
        else
          done('Unexpected result')
      )


  describe 'deleteFile', ->
    it 'should delete existing file in the cloud', (done) ->
      api.deleteFile(tempFileName, done)