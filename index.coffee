url       = require 'url'
_         = require 'lodash'
request   = require 'request'
FormData  = require 'form-data'

defaults = {
  host            : 'favoritecloud.com'
  namespace       : 'api/v1'
  ignoreEndpoints : false
  username        : null
  apiKey          : null
}

class FavoriteCloud
  constructor: (options = {}) ->
    @options = _.defaults(options, defaults)
    @_authHeader = "ApiKey #{@options.username}:#{@options.apiKey}"
    return this


  _buildURI: (endpoint) ->
    endpoint = '' if @options.ignoreEndpoints
    "http://#{@options.host}/#{@options.namespace}" + endpoint


  doRequest: (options, callback) ->
    options.uri = @_buildURI(options.uri)

    options.headers or (options.headers = {})
    options.headers['Authorization'] = @_authHeader

    request(options, callback)


  handleResponse: (callback) ->
    (error, response, body) ->
      if error
        console.error(error)
        console.stack(error)
        callback(error)

      else
        callback(null, response, body)


  getFilesList: (callback) ->
    @doRequest(
      method: 'GET'
      uri: '/files/'
      @handleResponse (err, res, body) ->
        return callback(err) if err
        json = JSON.parse(body)
        callback(null, json)
    )


  getFileInfo: (fileName, callback) ->
    @doRequest(
      method: 'GET'
      uri: '/files/' + fileName
      @handleResponse (err, res, body) ->
        return callback(err) if err

        json = JSON.parse(body)
        callback(null, json)
    )


  getFile: (fileName, callback) ->
    @getFileInfo(fileName, (err, fileInfo) ->
      return callback(err) if err

      if fileURI = fileInfo.publicURI
        callback(null, request(fileURI))
      else
        callback('No file URI')
    )


  createFile: (fileStream, options, callback) ->
    if _.isFunction(options)
      callback = options
      options = {}

    form = new FormData
    form.append('content', fileStream, options)

    form.getLength (err, length) =>
      req = @doRequest(
        method: 'POST'
        uri: '/files/'
        headers: {
          'content-length'  : length
        }
        @handleResponse (err, res, body) ->
          return callback(err) if err

          json = JSON.parse(body)
          callback(null, json)
      )

      req._form = form


  replaceFile: (fileName, fileStream, callback) ->
    callback('Not implemented in API yet')
#    form = new FormData
#    form.append('content', fileStream)
#
#    form.getLength (err, length) =>
#      req = @doRequest(
#        method: 'PUT'
#        uri: '/files/' + fileName
#        headers: {
#          'content-length'  : length
#        }
#        @handleResponse (err, res, body) ->
#          return callback(err) if err
#
#          json = JSON.parse(body)
#          callback(null, json)
#      )
#
#      req._form = form


  deleteFile: (fileName, callback) ->
    @doRequest(
      method: 'DELETE'
      uri: '/files/' + fileName
      @handleResponse (err) -> callback(err)
    )

module.exports = FavoriteCloud